package at.spenger.junit.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.stream.Collectors;
import java.util.Map;


public class Club {
	private List<Person> l;
	
	public Club() {
		l = new ArrayList<>();
	}
	
	public boolean enter(Person p) {
		if (p == null) {
			throw new IllegalArgumentException("person is null!");
		}
		return l.add(p);
	}
	
	public int numberOf() {
		return l.size();
	}
	
	public List<Person> getPersons() {
		return Collections.unmodifiableList(l);
	}
	
	public double getDurchschnitt()
	{
		double avg = l.parallelStream()
							.mapToDouble(u -> u.getAge())
							.average()
							.getAsDouble();
		
		return avg;
	} 
	
	public void sort(){
		
		l
		.stream()
		.sorted((e1,e2)->e1.getLastName().compareTo(e2.getLastName()))
		.sorted((e1,e2)->Integer.compare(e1.getAge(),e2.getAge()))
		.forEach(e->System.out.println(e));
   
	}
	
	public void groupByBirthday()
	{
		Map<String,List<Person>> groupByBirthday = (Map<String, List<Person>>) l.stream().collect(Collectors.groupingBy(Person::getBirthYear));
		System.out.println(groupByBirthday);
	}
	
	public boolean leave(int index)
	{
		return l.remove(index) != null;
	}

}
