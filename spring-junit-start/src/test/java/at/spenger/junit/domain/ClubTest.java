package at.spenger.junit.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class ClubTest {

	@Test
	public void testEnter()
	{
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Club c =new Club();
		boolean erwartet = true;
		boolean erg=c.enter(p);
		assertEquals(erwartet, erg);
	}
	@Test
	public void testNumberOF()
	{
		Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
		Club c =new Club();
		c.enter(p);
		int erwartet = 1;
		int erg = c.numberOf();
		assertEquals(erwartet, erg);
	}
	
	 @Test
	 public void testLeave(int index)
	 {
		 Person p = new Person("James", "Bond", LocalDate.parse("1950-09-27"), Person.Sex.MALE);
			Club c =new Club();
			c.enter(p);
			c.leave(0);
			boolean erwartet = true;
			boolean erg=c.leave(0);
			assertEquals(erwartet,erg);
	 }
	 

	@Test
	 public void testGetDurchschnitt(){
		 Person p1 = new Person("James", "Bond", LocalDate.parse("1954-09-27"), Person.Sex.MALE);
		 Person p2 = new Person("James", "Bond", LocalDate.parse("1955-09-27"), Person.Sex.MALE);
		 Person p3 = new Person("James", "Bond", LocalDate.parse("1956-09-27"), Person.Sex.MALE);
				
		 Club c =new Club();
			c.enter(p1);
			c.enter(p2);
			c.enter(p3);
			c.getDurchschnitt();
			double erwartet = 59;
			double erg = c.getDurchschnitt();
			assertEquals(erwartet,erg);
	 }
	
	

}
